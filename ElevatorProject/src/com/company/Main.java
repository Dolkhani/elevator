package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Message to user to input destination floor
        System.out.println(" +++++++++ Welcome to elevator's user interface +++++++++\n"
                +"/////////////////////////////////////////////////////////////\n");
        // Create instance of Scanner class and allocate it to variable "sc" which has Scanner type
        Scanner sc = new Scanner(System.in);
        // read the next line entry as elevator name by help of scanner
        System.out.println("Please enter elevator name.");
        String elevatorName = sc.nextLine();
        //Receive floor which is elevator start floor
        System.out.println("Please enter elevator start floor.");
        int elevatorStartFloor = sc.nextInt();
        // Create instance of Elevator class and allocate it to variable "elevator" which has Elevator type
        Elevator elevator = new Elevator(elevatorName, elevatorStartFloor);
        //while(true) {
        // Floor which user wants to go
        int destinationFloor = 0;
        System.out.println("Current floor is: " + elevator.getCurrentFloor());
        // Message to user to input destination floor
        System.out.println("Please enter your destination floor.");
        // read the next integer entry as destinationFloor by help of scanner
        destinationFloor = sc.nextInt();
        //Give user confirmation for receiving entry
        System.out.println("Destination floor: " + destinationFloor );
        //Close Scanner
        //sc.close();
        // If user in the same floor
        elevator.goTo(destinationFloor);
        //}
        //Close Scanner
        sc.close();
        System.out.println("Bye from elevator: " + elevator );
    }
}
