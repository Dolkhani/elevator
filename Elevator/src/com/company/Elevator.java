package com.company;

public class Elevator {
    private String elevatorName;
    private int currentFloor;
    private int lowestFloor = 0;	// Constant
    private int highestFloor = 99;	// Constant

    public Elevator(String elevatorName, int elevatorStartFloor) {
        this.elevatorName = elevatorName;
        this.currentFloor = elevatorStartFloor;
    }

    public void goTo(int destinationFloor) {
        if(destinationFloor < lowestFloor) {
            System.out.println("Your destination is lower than lowest floor.");
            return;
        }

        if(destinationFloor > highestFloor) {
            System.out.println("Your destination is higher than highest floor.");
            return;
        }


        if(destinationFloor == currentFloor) {
            System.out.println("Your are in the same floor.");
            return;
        }

        if(destinationFloor > currentFloor) {
            goUp(destinationFloor - currentFloor);
            return;
        }

        if(destinationFloor < currentFloor) {
            goDown(currentFloor - destinationFloor );
            return;
        }
    }


    // Elevator goUp method
    void goUp(int numberOfFloorsToGoUP) {
        while(numberOfFloorsToGoUP > 0) {
            currentFloor = currentFloor + 1;
            System.out.println("\"Pling ... at floor " + currentFloor +".\"");
            numberOfFloorsToGoUP = numberOfFloorsToGoUP - 1 ;
        }
    }
    // Elevator go down method
    void goDown(int numberOfFloorsToGoDown) {
        while(numberOfFloorsToGoDown > 0) {
            currentFloor = currentFloor - 1;
            System.out.println("\"Pling ... at floor " + currentFloor +".\"");
            numberOfFloorsToGoDown = numberOfFloorsToGoDown - 1 ;
        }
    }


    public String getElevatorName() {
        return elevatorName;
    }

    public void setElevatorName(String elevatorName) {
        this.elevatorName = elevatorName;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public int getLowestFloor() {
        return lowestFloor;
    }

    public void setLowestFloor(int lowestFloor) {
        this.lowestFloor = lowestFloor;
    }

    public int getHighestFloor() {
        return highestFloor;
    }

    public void setHighestFloor(int highestFloor) {
        this.highestFloor = highestFloor;
    }

    @Override
    public String toString() {
        return "Elevator [name = " + elevatorName + " , currentFloor = " + currentFloor + " ]";
    }


}